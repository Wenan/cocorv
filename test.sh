set -eu
test() {
  controlCount=./example/1KG/chr21.gnomAD.annotated.vcf.gz.gds
  caseCount=./example/1KG/chr21.samples.annotated.GT.vcf.gz.gds
  sampleList=./example/1KG/samples.txt
  intersectBed=./example/1KG/intersect.coverage10x.bed.gz
  variantExclude=./example/1KG/gnomAD.exclude.allow.segdup.lcr.v3.txt.gz
  AFMax=1e-4
  maxAFPopmax=1
  variantMissing=0.1
  ACANConfig=./example/1KG/stratified_config_gnomad.txt
  ancestryFile=./example/1KG/ethnicity.txt
  variantGroup="annovar_pathogenic" 
  REVELThreshold=0.65
  pLDControl=0.05
  highLDVariantFile=./example/1KG/full_vs_gnomAD.p0.05.OR1.ignoreEthnicityInLD.rds
  fileID=21
  outputPrefix=./example/1KG/result/AFMax${AFMax}.${variantGroup}.${REVELThreshold}.excludeV3.LDv2.chr${fileID}

  mkdir -p ./example/1KG/result/

  Rscript utilities/CoCoRV_wrapper.R \
    --sampleList ${sampleList} \
    --outputPrefix ${outputPrefix} \
    --AFMax ${AFMax} \
    --maxAFPopmax ${maxAFPopmax} \
    --bed ${intersectBed} \
    --variantMissing ${variantMissing} \
    --variantGroup ${variantGroup} \
    --removeStar \
    --ACANConfig ${ACANConfig} \
    --caseGroup ${ancestryFile} \
    --minREVEL ${REVELThreshold} \
    --variantExcludeFile ${variantExclude}  \
    --checkHighLDInControl \
    --pLDControl ${pLDControl} \
    --highLDVariantFile ${highLDVariantFile} \
    --fullCaseGenotype \
    --fileID ${fileID} \
    ${controlCount} \
    ${caseCount}

  echo "QQ plot of the dominant model, and estimated FDR"
  outputFile=${outputPrefix}.association.tsv
  Rscript utilities/QQPlotAndFDR.R ${outputFile} \
           ${outputFile}.dominant.nRep1000 --setID gene \
           --outColumns fileID,gene --n 1000 \
      --pattern "case.*Mutation.*_DOM$|control.*Mutation.*_DOM$" \
      --nullBoxplot --FDR
}

main() {
  test
}

main "$@"
