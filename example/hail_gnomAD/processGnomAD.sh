# This script processes different versions of gnomAD

############ Tools ##############################################
module load bedtools
module load htslib
# very important to use the same annotation for both the cases and controls
annovarFolder="/research/groups/cab/projects/Control/common/reference/annovar_2019-10-24" 
#################################################################

####################### gnomAD raw data sets ########################
# gnomAD v4.0.0 exomes
vcfPrefix="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4/exomes/gnomad.exomes.v4.0.sites.chr"
vcfSuffix=".vcf.bgz"
outputRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4/exomes/processed/"
refbuild="hg38" 
coverage="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4/gnomad.exomes.v4.0.coverage.summary.tsv.bgz"

# gnomAD v4.0.0 genomes
vcfPrefix="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4/genomes/gnomad.genomes.v4.0.sites.chr"
vcfSuffix=".vcf.bgz"
outputRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4/genomes/processed/"
refbuild="hg38" 
coverage="" # not available yet

# gnomad v3.1.2 genome hg38
vcfPrefix="/rgs01/reference/public/gnomad/broadinstitute/r3.1.1/gnomad.genomes.v3.1.1.sites.chr"
vcfSuffix=".vcf.bgz"
coverage="/research/groups/cab/projects/Control/common/reference/gnomAD/r3.0.1/coverage/genomes/gnomad.genomes.r3.0.1.coverage.summary.tsv.bgz"
outputRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r3/genomes/"
refbuild="hg38" 

# gnomad v2.1.1 exome hg19
vcfPrefix="/research/groups/cab/projects/Control/common/reference/gnomAD/r2.1.1/vcf/exomes/gnomad.exomes.r2.1.1.sites."
vcfSuffix=".vcf.bgz"
coverage="/research/groups/cab/projects/Control/common/reference/gnomAD/r2.1/coverage/exomes/gnomad.exomes.coverage.summary.tsv.bgz"
outputRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r2.1.1/exomes/"
refbuild="hg19" 

# gnomad v2.1.1 genome hg19
vcfPrefix="/research/groups/cab/projects/Control/common/reference/gnomAD/r2.1.1/vcf/genomes/gnomad.genomes.r2.1.1.sites."
vcfSuffix=".vcf.bgz"
coverage="/research/groups/cab/projects/Control/common/reference/gnomAD/r2.1/coverage/genomes/gnomad.genomes.coverage.summary.tsv.bgz"
outputRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r2.1.1/genomes/"
refbuild="hg19" 

#################################################################



# generate the bed file with coverage at least 10 for at least 90% samples
bedFile=${outputRoot}/coverage10x.bed.gz
zcat ${coverage} | awk '(NR > 1 && $7 > 0.9) {print $1}' | sed 's/:/ /' | \
awk '{print $1,$2-1,$2}' | sort -k1,1 -k2,2n | sed 's/ /\t/g' | bedtools merge -i stdin \
| gzip > ${bedFile}

# This annotates the variants using ANNOVAR
queue="priority"
memory="100000" # 20G for gnomADV4 WES, 100G for WGS
outputFolder="${outputRoot}/annotate"
mkdir -p ${outputFolder}
if [[ ${refbuild} == "hg38" ]]; then
  protocol="refGene,gnomad211_exome1,gnomad30_genome1,exac03,exac03nontcga,1000g2015aug_all,esp6500siv2_all,dbnsfp41a,intervar_20180118,dbscsnv11,clinvar_20210919,revel"
  operation="g,f,f,f,f,f,f,f,f,f,f,f"
elif [[ ${refbuild} == "hg19" ]]; then
  protocol="refGene,exac03,exac03nontcga,1000g2015aug_all,esp6500siv2_all,dbnsfp41a,intervar_20180118,dbscsnv11,clinvar_20210919,revel"
  operation="g,f,f,f,f,f,f,f,f,f"
fi
for chr in {1..22} X Y; do
  vcf=${vcfPrefix}${chr}${vcfSuffix}
  bsub -q ${queue} -M ${memory} -oo ${outputFolder}/${chr}.o \
    -eo ${outputFolder}/${chr}.e \
  bash /home/wchen1/tools/CoCoRV/utilities/annotate.sh \
    ${vcf} ${annovarFolder} \
    ${refbuild} ${outputFolder}/chr${chr}.annovar NA NA ${protocol} ${operation}
done
#
# check Success
# gnomAD V2 gnome has only 23 chromosomes
if [[ $(grep Succ ${outputFolder}/*.o | wc -l) == 24 ]]; then
  echo "Succeed"
fi

# This annotates the variants using VEP
# It taks about six days for gnomADV4 chr1 exomes
module load perl/5.28.1
module load htslib/1.10.2
module load bcftools/1.15.1
module load samtools/1.10
outputFolder=${outputRoot}/vcfWithVEPJob/
mkdir -p ${outputFolder}
if [[ ${refbuild} == "hg38" ]]; then
  GRChID="GRCh38"
  vepFolder="/research/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep"
  cache="/research/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep/cache_hg38"
  lofteeFolder="/research/groups/cab/projects/Control/common/reference/VEP/other_data/loftee/loftee"
  lofteeDataFolder="/research/groups/cab/projects/Control/common/reference/VEP/other_data/loftee/data_hg38"
  caddSNV="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/CADD/hg38/whole_genome_SNVs.tsv.gz"
  caddIndel="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/CADD/hg38/gnomad.genomes.r3.0.indel.tsv.gz"
  spliceAISNV="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/SpliceAI/spliceai_scores.raw.snv.hg38.vcf.gz"
  spliceAIIndel="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/SpliceAI/spliceai_scores.raw.indel.hg38.vcf.gz"
  perlThread="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep/cache_hg38/cpanm/lib/perl5/x86_64-linux-thread"
  AM="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/AlphaMissense"
  REVEL="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/REVEL"
  reference="/research/groups/cab/projects/Control/common/reference/Homo_sapiens/GRCh38_no_alt/FASTA/GRCh38_no_alt.fa"
elif [[ ${refbuild} == "hg19" ]]; then
  GRChID="GRCh37"
  vepFolder="/research/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep"
  cache="/research/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep/cache"
  lofteeFolder="/research/groups/cab/projects/Control/common/reference/VEP/other_data/loftee/loftee"
  lofteeDataFolder="/research/groups/cab/projects/Control/common/reference/VEP/other_data/loftee/data"
  caddSNV="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/CADD/hg19/whole_genome_SNVs.tsv.gz"
  caddIndel="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/CADD/hg19/InDels.tsv.gz"
  spliceAISNV="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/SpliceAI/spliceai_scores.raw.snv.hg19.vcf.gz"
  spliceAIIndel="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/SpliceAI/spliceai_scores.raw.indel.hg19.vcf.gz"
  AM="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/AlphaMissense"
  REVEL="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/other_data/REVEL"
  reference="/research/groups/cab/projects/Control/common/reference/Homo_sapiens/GRCh37-lite/FASTA/GRCh37-lite.fa"
  perlThread="/research_jude/rgs01_jude/groups/cab/projects/Control/common/reference/VEP_v103/ensembl-vep/cache_hg38/cpanm/lib/perl5/x86_64-linux-thread"
fi

annotateVEP="/home/wchen1/tools/CoCoRV/utilities/annotateVEPWithOptions.sh"
queue="priority" #"standard"
nThreads=8
annotations="AM,SPLICEAI,CADD,LOFTEE"
for chr in {1..22} X Y; do
  inVCF=${outputRoot}/annotate/chr${chr}.annovar.vcf.gz
  outVCFPrefix=${outputRoot}/annotate/chr${chr}.annovar.vep
  bsub -q ${queue} -M 20000 \
    -n ${nThreads} -R "span[hosts=1]" -P para \
    -oo ${outputFolder}/${chr}.vep.o \
    -eo ${outputFolder}/${chr}.vep.e \
  bash ${annotateVEP} ${inVCF} ${GRChID} ${outVCFPrefix} ${reference} ${vepFolder} ${cache} ${lofteeFolder} ${lofteeDataFolder} ${caddSNV} ${caddIndel} ${spliceAISNV} ${spliceAIIndel} ${perlThread} ${AM} ${REVEL} ${nThreads} ${annotations} 2> ${outputFolder}/vep.${chr}.log
done
#
# 22 if not including X and Y
if [[ "24" == $(grep 'Succ' ${outputRoot}/vcfWithVEPJob//*.vep.o | wc -l) ]]; then
  echo "Succeed"
fi

# convert vcf with counts to gds
queue="priority" #"standard"
outputFolder=${outputRoot}/countvcf2gdsJob/
mkdir -p ${outputFolder}
for chr in {1..22} X Y; do
  vcf=${vcfPrefix}${chr}${vcfSuffix}
  gds=${outputRoot}/$(basename ${vcf}).gds
  bsub -q ${queue} -M 200000 -oo ${outputFolder}/${chr}.o \
  -eo ${outputFolder}/${chr}.e \
  "module load R/4.2.0-rhel8 && Rscript /home/wchen1/tools/CoCoRV/utilities/vcf2gds.R ${vcf} ${gds} 1"
done
#
# check Success
if [[ "24" == $(grep 'Succ' ${outputRoot}/countvcf2gdsJob/*.o | wc -l) ]]; then
  echo "Succeed"
fi

# convert vcf with annotations to gds
queue="priority" #"standard"
outputFolder=${outputRoot}/vcf2gdsJob/
annotatedPrefix=${outputRoot}/annotate/chr
annotatedSuffix=".annovar.vep.vcf.gz"
mkdir -p ${outputFolder}
for chr in {1..22} X Y; do
  vcf=${annotatedPrefix}${chr}${annotatedSuffix}
  gds=${vcf}.gds
  bsub -q ${queue} -M 40000 -oo ${outputFolder}/${chr}.o \
  -eo ${outputFolder}/${chr}.e \
  "module load R/4.2.0-rhel8 && Rscript /home/wchen1/tools/CoCoRV/utilities/vcf2gds.R ${vcf} ${gds} 1"
done
#
# check Success
if [[ "24" == $(grep 'Succ' ${outputRoot}/vcf2gdsJob/*.o | wc -l) ]]; then
  echo "Succeed"
fi

#### download ancestry group classificaton ####
# outputFolder=${outputRoot}/ancestry
# mkdir -p ${outputFolder}
# module load gsutil
# gsutil cp gs://gcp-public-data--gnomad/release/4.0/pca/* ${outputFolder}

## generate variants to exclude other than indicated by FILTER for gnomADv4.1
#
# gnomADv4.1 genomes
# no extra variants so far
# 
# gnomADv4.1 exomes
# add variants failed in gnomADv4.1 genomes within the WES intervals 
# but not in failed gnomADv4.1 exomes (trust WGS more than WES)
# make sure to download the interval file: 
# ukb.pad50.broad.pad50.union.intervals 
queue="priority"
dataRoot="/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4.1.0/"
outputFolder=/research/groups/cab/projects/Synthetic_Control/common/WGS/Raw/rare_variant_analysis/gnomADAnalysis2/r4.1.0/exomes/processed/failed

sed -e 's/:\|-/\t/g' ${dataRoot}/exomes/ukb.pad50.broad.pad50.union.intervals \
  | sort -k1,1V -k2,2n > ${dataRoot}/exomes/ukb.pad50.broad.pad50.union.intervals.tsv

mkdir -p ${outputFolder}
for chr in {1..22} X Y; do
  wes="${dataRoot}/exomes/gnomad.exomes.v4.1.sites.chr${chr}.vcf.bgz"
  bsub -q ${queue} -M 8000 -oo ${outputFolder}/${chr}.o \
  -eo ${outputFolder}/${chr}.e \
  bcftools query -i'%FILTER!="PASS"' -f'%CHROM-%POS-%REF-%ALT\n' $wes -o ${outputFolder}/WES.chr${chr}.failed.txt
done
for chr in {1..22} X Y; do
  wgs="${dataRoot}/genomes/gnomad.genomes.v4.1.sites.chr${chr}.vcf.bgz"
  bsub -q ${queue} -M 8000 -oo ${outputFolder}/${chr}.WGS.o \
  -eo ${outputFolder}/${chr}.WGS.e \
  bcftools query -R ${dataRoot}/exomes/ukb.pad50.broad.pad50.union.intervals.tsv -i'%FILTER!="PASS"' -f'%CHROM-%POS-%REF-%ALT\n' $wgs -o ${outputFolder}/WGS.chr${chr}.failed.txt
done

# keep only those in failed WGS within the exon intervals but not recorded in 
# failed WES.
for chr in {1..22} X Y; do
  wesFailed=${outputFolder}/WES.chr${chr}.failed.txt
  wgsFailed=${outputFolder}/WGS.chr${chr}.failed.txt
  comm -23 <(sort $wgsFailed) <(sort $wesFailed) > ${outputFolder}/WGSUniqueInCoding.chr${chr}.failed.txt
done

cat ${outputFolder}/WGSUniqueInCoding.chr*.failed.txt | gzip > gnomAD41WGSExtraExcludeInCoding.txt.gz
