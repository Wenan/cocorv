import hail as hl
import sys
import re

loadingPath = sys.argv[1]
rfModelPath = sys.argv[2]
vcf = sys.argv[3]
build = sys.argv[4]
outputFile = sys.argv[5]
threshold = float(sys.argv[6])

loadings_ht = hl.read_table(loadingPath)

# import vcf for projection and gnomAD loadings Hail Table
mt_to_project = hl.import_vcf(vcf, reference_genome = build)

# Project new genotypes onto loadings
ht = hl.experimental.pc_project(
    mt_to_project.GT,
    loadings_ht.loadings,
    loadings_ht.pca_af,
)

# Assign global ancestry using the gnomAD RF model and PC project scores
# Loading of the v2 RF model requires an older version of scikit-learn, this can be installed using pip install -U scikit-learn==0.21.3
if re.search(".*.pkl", rfModelPath) is not None:  # use pkl format
    import pickle
    from gnomad.sample_qc.ancestry import assign_population_pcs

    with hl.hadoop_open(rfModelPath, "rb") as f:
        fit = pickle.load(f)
    # Reduce the scores to only those used in the RF model, this was 6 for v2 and 16 for v3.1, 20 for v4

    num_pcs = 0
    if hasattr(fit, "n_features_in_"):
        num_pcs = fit.n_features_in_
    elif hasattr(fit, "n_features_"):
        num_pcs = fit.n_features_
    else:
        sys.exit("attribute n_features_in_ or n_features_ not exist")

    ht = ht.annotate(scores=ht.scores[:num_pcs])
    ht, rf_model = assign_population_pcs(
        ht,
        pc_cols=ht.scores,
        fit=fit,
        min_prob=threshold,
    )
elif re.search(".*.onnx", rfModelPath) is not None: # use onnx format
    import onnx
    from gnomad.sample_qc.ancestry import apply_onnx_classification_model,assign_population_pcs

    import onnxruntime as ort
    _default_session_options = ort.capi._pybind_state.get_default_session_options()
    def get_default_session_options_new():
         _default_session_options.inter_op_num_threads = 1
         _default_session_options.intra_op_num_threads = 1
         return _default_session_options
    ort.capi._pybind_state.get_default_session_options = get_default_session_options_new

    with hl.hadoop_open(rfModelPath, "rb") as f:
        fit = onnx.load(f)
    # Need to figure out the number of features used in the model
    num_pcs = fit.graph.input[0].type.tensor_type.shape.dim[1].dim_value
    ht = ht.annotate(scores=ht.scores[:num_pcs])
    ht, rf_model = assign_population_pcs(
        ht,
        pc_cols=ht.scores,
        fit=fit,
        min_prob=threshold,
        apply_model_func=apply_onnx_classification_model,
    )
else: 
    sys.exit("expect pkl or onnx as the random forest model") 

# The returned Hail Table includes the imputed population labels and RF probabilities for each gnomAD global population

# export the result
ht.export(outputFile)

