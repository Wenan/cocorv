#!/bin/bash

set -eu -o pipefail


main() {
  # calculate the coverage with specified parameters
  # also make sure the positions are exactly those in the bed file

  bamFile=$1       # input bam file
  outputPrefix=$2  # the output prefix, coverage output will be 
                   # ${outputPrefix}.coverage.gz
  bedFile=$3       # the bed file that specifies the region to calculate the 
                   # covearge
  SAMTOOLS=$4      # the path of samtools excutive file
  
  filename=$(basename -- "$bamFile")
  extension="${filename##*.}"
  
  cramOption=""
  if [[ ${extension} == "cram" ]]; then
    reference=$5   # the reference file for the cram format
    cramOption="--reference ${reference}"
  fi

  minBaseQuality=10
  minMappingQuality=20

  echo ${bamFile} > ${outputPrefix}.list.txt
  outputFile=${outputPrefix}.coverage.gz

  ${SAMTOOLS} depth -f ${outputPrefix}.list.txt ${cramOption} \
    -q ${minBaseQuality} -Q ${minMappingQuality} -b ${bedFile} -a \
   | awk '{print $3}' | gzip > ${outputFile}
}

main "$@"

