set -euo pipefail

main() {
# usage: bash <script> ${vcfFile} ${ASSEMBLY} ${outputPrefix} ${REF} ${VEPDIR} ${VEPCACHE} ${LOFTEEDIR} ${LOFTEEDATADIR} ${CADDSNVS} ${CADDINDELS} ${SPLICEAISNVS} ${SPLICEAIINDELS} ${PERL5} ${AlphaMissenseDIR} ${REVELDIR} ${nThreads}


### HANDLE INPUTS
vcfFile=$1
ASSEMBLY=$2
outputPrefix=$3
REF=$4
VEPDIR=$5
VEPCACHE=$6
LOFTEEDIR=$7
LOFTEEDATADIR=$8
CADDSNVS=$9
CADDINDELS=${10}
SPLICEAISNVS=${11}
SPLICEAIINDELS=${12}
PERL5=${13}
AlphaMissenseDIR=${14}
REVELDIR=${15}
nThreads=${16}

### SET VARIABLES
if [ $ASSEMBLY == 'GRCh38' ]; then
    LOFTEE="LoF,loftee_path:${LOFTEEDIR},human_ancestor_fa:${LOFTEEDATADIR}/human_ancestor.fa.gz,conservation_file:${LOFTEEDATADIR}/loftee.sql,gerp_bigwig:${LOFTEEDATADIR}/gerp_conservation_scores.homo_sapiens.GRCh38.bw"
	AlphaMissense="AlphaMissense,file=${AlphaMissenseDIR}/hg38/AlphaMissense_hg38.tsv.gz"
	REVEL="REVEL,file=${REVELDIR}/new_tabbed_revel_grch38.tsv.gz,no_match=1"
elif [ $ASSEMBLY == 'GRCh37' ]; then
    LOFTEE="LoF,loftee_path:${LOFTEEDIR},human_ancestor_fa:${LOFTEEDATADIR}/human_ancestor.fa.gz,conservation_file:${LOFTEEDATADIR}/phylocsf_gerp.sql,gerp_file:${LOFTEEDATADIR}/GERP_scores.final.sorted.txt.gz"
	AlphaMissense="AlphaMissense,file=${AlphaMissenseDIR}/hg19/AlphaMissense_hg19.tsv.gz"
	REVEL="REVEL,file=${REVELDIR}/new_tabbed_revel.tsv.gz,no_match=1"
else
    echo "GENOME VERSION PROVIDED IS INVALID!"
    exit 1;
fi

CADD="CADD,${CADDSNVS},${CADDINDELS}"
SPLICEAI="SpliceAI,snv=${SPLICEAISNVS},indel=${SPLICEAIINDELS}"
export PERL5LIB=${VEPCACHE}/Plugins:${PERL5}

### RUN VEP
${VEPDIR}/vep --cache --dir ${VEPCACHE} \
	--offline --vcf --assembly ${ASSEMBLY} \
	--pick \
	--plugin ${LOFTEE} \
	--plugin ${CADD} \
	--plugin ${SPLICEAI} \
	--plugin ${AlphaMissense} \
	--plugin ${REVEL} \
	--force_overwrite \
	--compress_output bgzip \
	-i ${vcfFile} \
	-o ${outputPrefix}.tmp.vcf.gz \
	--fork ${nThreads} \
	--fasta ${REF} 2> ${outputPrefix}_vep.log


## IMPROVE VEP ANNOTATION BY SPLITTING
bcftools +split-vep \
	-c SYMBOL,Consequence,LoF,LoF_filter,LoF_flags,LoF_info,CADD_PHRED,CADD_RAW,SpliceAI_pred_DP_AG,SpliceAI_pred_DP_AL,SpliceAI_pred_DP_DG,SpliceAI_pred_DP_DL,SpliceAI_pred_DS_AG,SpliceAI_pred_DS_AL,SpliceAI_pred_DS_DG,SpliceAI_pred_DS_DL,am_class,am_pathogenicity,REVEL \
	-Oz -o ${outputPrefix}.tmp.vep.vcf.gz ${outputPrefix}.tmp.vcf.gz
tabix -p vcf ${outputPrefix}.tmp.vep.vcf.gz

### APPLY CLEANED ANNOTATIONS
bcftools annotate -a ${outputPrefix}.tmp.vep.vcf.gz \
	--collapse none \
	-c CSQ,SYMBOL,Consequence,LoF,LoF_filter,LoF_flags,LoF_info,CADD_PHRED,CADD_RAW,SpliceAI_pred_DP_AG,SpliceAI_pred_DP_AL,SpliceAI_pred_DP_DG,SpliceAI_pred_DP_DL,SpliceAI_pred_DS_AG,SpliceAI_pred_DS_AL,SpliceAI_pred_DS_DG,SpliceAI_pred_DS_DL,am_class,am_pathogenicity,REVEL \
	-Oz -o ${outputPrefix}.vcf.gz ${vcfFile}
tabix -p vcf ${outputPrefix}.vcf.gz

### CLEAN UP
rm ${outputPrefix}.tmp.vcf.gz* ${outputPrefix}.tmp.vep.vcf.gz*

}

main "$@"
