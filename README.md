# Consistent summary Count based Rare Variant burden test (CoCoRV) #

### What is this repository for? ###
This repository include tools for performing consistent summary count based rare variant burden test, which is useful when we only have sequenced cases data. For example, we can compare the cases against public summary count data, such as gnomAD. Consistent filtering is applied to make sure the same set of high quality variants are used. It can stratify cases into different ethnicity groups, and perform stratified analysis with group-matched control summary counts. For recessive models, it can exclude double heterozygous due to high linkage disequilibrium in populations. This package also provides accurate inflation factor estimate, QQ plot, and powerful FDR control for discrete count data, whose p-value distribution under the null is far from the uniform distribution when the alleles are very rare.     

### Install and a quick test ###
#### Install micromamba ####
We use micromamba to install all dependent tools and R packages except discreteMTP. 
```bash
# assume existing ~/bin
cd ~
curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj bin/micromamba 
# this writes to your home .bashrc file and creates a micromamba folder in your home directory which will contain all of your conda environments created by micromamba
./bin/micromamba shell init -s bash -p ~/micromamba  
```
Then restart the terminal.
#### Install dependencies ####
Download or clone the cocorv package, change current directory to the directory of the package.  
```bash
# assume to put micromamba packages in ../micromamba_tools and pip cache in ../pip_cache
micromamba create --root-prefix ../micromamba_tools -f ./utilities/cocorv-env-gnomad.yaml
export MAMBA_ROOT_PREFIX=$(readlink -f ../micromamba_tools)
micromamba activate cocorv-env-gnomad
pip install --cache-dir ../pip_cache gnomad==0.7.1 hail==0.2.128
```
To install the R packages DiscreteFDR and discreteMTP, start R and then type the following
```R
install.packages("DiscreteFDR")
install.packages("https://cran.r-project.org/src/contrib/Archive/discreteMTP/discreteMTP_0.1-2.tar.gz")
```
#### Install CoCoRV ####
Install the CoCoRV R package, and do a quick test. 
```bash
# install the R package
Rscript build.R

# this test is a toy example of summary count based rare variant burden test, results are saved in ./example/1KG/result/. 
bash test.sh

# Optional for efficient generation of coverage summary statistics
# Requires python 3.7 or later 
module load python/3.7.0
pip install pyBigWig
```

### Output Files ###
From the above toy example, there should be five output files. 

* association file: the suffix is ".association.tsv". This is the main assocaiton test result file. DOM means the dominant model, REC means the recessive model and 2HETS means the double heterozygous model. For each mdoel, the columns are group by each ethnicity, with the same number of columns. The following shows the result of the dominant model and the columns corresponding to the NFE (nonFinnish European):
    * gene: the gene ID
    * P_DOM: the p-value of the dominant model
    * OR_DOM: the odds ratio of the dominant model
    * caseWMutation_NFE_DOM:  the number of samples with the variants of interest in cases
    * caseWOMutation_NFE_DOM: the number of samples without the variants of interest in cases
    * controlWMutation_NFE_DOM: the number of samples with the variants of interest in controls
    * controlWMutation_NFE_DOM: the number of samples without the variants of interest in controls 
    * other columns: other columns with the column name pattern "caseEstimated*" are used for debug and comparison, can be ignored in practical usage
* fdr file: the suffix is ".fdr.tsv". It adds FDRs calcualted from 5 different methods:
    * RBH_point_estimate: the resampling based method using the point estimate
    * RBH_uppper_limit: the resampling based method using the upper limit estimate
     DBH: discrete count based FDR calcualted from the R package discreteMTP
    * DBH.sd, ADBH.sd: discrete count based FDR calcualted from the R package DiscreteFDR. These five FDR estimates are often similar. DBH.sd, ADBH.sd seem to be good FDR choices. 
* QQ plot and lambda estimate: the suffix is ".pdf". If the option --nullBoxplot is used, it will add the boxplot of the lambda on the right side under the null of no associations.
* variants afer QC in cases: the suffix is ".case.group". These show the variants in each gene in cases after consistent QC and filtering.
* variants afer QC in controls: the suffix is ".control.group". These show the variants in each gene in controls after consistent QC and filtering.    


### A toy example using whole exome sequencing samples from the 1000 Genomes ###
We assume that the VCF file of cases has already been generated and variant quality check has been done. For example, we can use VQSR from GATK to keep high quality variants while allowing only a small fraction of false positives. Here we use a subset of 1000 Genomes whole exome sequencing data and chromosome 21 as a toy example, and do assocation test against the gnomAD summary counts.

#### Generate regions with good coverage ####
For both cases and controls, we restrict the analysis to the region where >= 90% of samples have coverage of depth >=10. For gnomAD, this bed file can be generated from the downloaded coverage summary statistics from here (https://gnomad.broadinstitute.org/downloads#v2-coverage, summary TSV files).  
```bash
# download gnomAD coverage data first, then run the following
zcat ${gnomADCoverage} | awk '(NR > 1 && $7 >= 0.9) {print $1,$2 - 1,$2}' \
  | sort -k1,1 -k2,2n | sed 's/ /\t/g' | bedtools merge -i stdin | gzip > ${bedFile}
```

For calculating the coverage summary statistics from cases, we use a scalable approach which can be easily parallelized. First we use samtools to generate the bigwig coverage file for each sample from the bam file over the regions specified. This step can be parallelized for a large number of samples. Then we sequentially accumulate the coverage information to generate the summary statistics. In this way, we can generate all needed percentage information for each coverage threshold. The only summary information missed is the median coverage which cannot be calculated sequentially.
```bash
# assume ${bedFile} is a BED file specifying the regions of interest, e.g., coding regions
# for each bam file, run the following. This part can be parallelized
bash ./BamToBW_CoverageSummary/bamToBW.sh ${bamFile} ${outputPrefix} ${bedFile} ${samtools} ${referenceBuild}

# Once all coverage files are generated, aggregate these coverages to generate covearge summary statistics
# ${bigwigFileList} is a one column file, each row corresponds to the file path of the generated bigwig file.
python ./BamToBW_CoverageSummary/bwToCoverageSummary.py -bw ${bigwigFileList} -bed ${bedFile} -out ${outputPrefix}

# generate the case bed file where 90% samples have coverage >= 10 
module load bedtools # load the bedtools for use
zcat ${outputFile} | \
      awk '(NR > 1 && \$7 >= 0.9) {print \$1,\$2 - 1,\$2}' \
    | sort -k1,1 -k2,2n | sed 's/ /\t/g' | bedtools merge -i stdin | gzip > samples.coverage10x.bed.gz
```
For more information, please check the file ./BamToBW_CoverageSummary/README.md

#### Intersection of good coverage bed file ####
Here we intersect the good coverage regions of both cases and controls
```bash
# assume bed files have been generated and gzipped
bedtools intersect -sorted -a ./example/1KG/samples.coverage10x.bed.gz -b ./example/1KG/gnomAD.coverage10x.bed.gz | bedtools merge -i stdin | gzip > \
        ./example/1KG/intersect.coverage10x.bed.gz
```

#### Excluded variants ####
We have created a black list of variants in gnomAD (./example/1KG/gnomAD.exclude.allow.segdup.lcr.v3.txt.gz). It will be given to the option --variantExcludeFile to exclude the variants in the black list. In addtional, those failed in the case QC or control QC based on the VCF filter field will also be excluded unless the option --ignoreFilter is set.

#### Precomouted LD Data ####
We have precomputed the LD test results either from gnomAD summary counts or from a full genotype cohort with samples from the 1,000 Genomes Project and the Alzheimer's Disease Sequencing Project (ADSP), see ./example/1KG/full_vs_gnomAD.p0.1.merged.tsv.gz. This file can be used directly with the option --highLDVariantFile. Alternatively, for quick loading and running, the tsv format can be processed and saved to an RDS file using the following:
```bash
LDTSVFile=./example/1KG/full_vs_gnomAD.p0.1.merged.tsv.gz
RDSFile="./example/1KG/full_vs_gnomAD.p0.05.OR1.ignoreEthnicityInLD.rds"
Rscript utilities/LDTSV2RDS.R ${LDTSVFile} ${RDSFile} \
   --pLDControl 0.05 --ORThresholdControl 1 --ignoreEthnicityInLD 
```

#### normalization and QC of the VCF ####
Assume the VCF file has been jointly called and GATK VQSR were used to assign the Filter status. The variant need to be normalized and further QCed. The normalization step standardizes the indels and splits multi-allelic variants into several biallelic variants. The QC step makes sure DP >=10 and GQ >= 20 for called genotypes, otherwise it is set to missing. It also makes sure the allele fraction for each heterozygous genotype is between 0.2 and 0.8. The following script should be helpful. Note that the file ./example/1KG/samples.vcf.gz has already been normalized and QCed but it does not hurt to apply the normalization and QC process again.
````bash 
vcfFile=./example/1KG/samples.vcf.gz 
outputPrefix=./example/1KG/samples
refFASTA="/research/rgs01/resgen/prod/tartan/index/reference/Homo_sapiens/GRCh37-lite/FASTA/GRCh37-lite.fa" # need to replace to your own path
# assume bcftools is installed
bash utilities/vcfQCAndNormalize.sh ${vcfFile} ${outputPrefix} ${refFASTA}
````     

#### Annotate the data ####
We provide a script to annotate a VCF file using ANNOVAR. Here we only added REVEL annotation in additiona to builtin gene annotations. However, many other annotations can be added, see the commented code in the script utilities/annotate.sh. It is also possible to use VEP for annotations and define custom R functions to restrict to the variant set of interest. See more in the help of CoCoRV in the R package for already defined sets of variants based on ANNOVAR.
```bash
annovarFolder="/path/to/annovar"
refbuild="hg19"  
vcfFile=./example/1KG/samples.vcf.gz
outputPrefix=./example/1KG/samples.annotated
bash utilities/annotate.sh ${vcfFile} ${annovarFolder} ${refbuild} ${outputPrefix}
# combine genotypes with annotations
vcfAnnotated=./example/1KG/samples.annotated.GT.vcf.gz
bcftools annotate -a ${outputPrefix}.vcf.gz -c INFO -Oz -o ${vcfAnnotated} ${vcfFile} 

# apply the same annotation pipeline to the controls
# the users need to download the chr21 gnomad exome data first from here: https://gnomad.broadinstitute.org/downloads
bash utilities/annotate.sh ${gnomADVCF} ${annovarFolder} ${refbuild} ./example/1KG/gnomAD.annotated
```

#### Estimate the population/ethnicity of case samples using gnomAD classifier ####
First we need to download gnomAD PCA variant loadings and random forest classifier model from gnomAD: v2(https://gnomad.broadinstitute.org/downloads#v2-ancestry-classification), v3 (https://gnomad.broadinstitute.org/downloads#v3-ancestry-classification), v4 (https://gnomad.broadinstitute.org/downloads#v4-genetic-ancestry-group-classification)

Then we run the following python script to extract PCs and gnomAD ancestry. 
```bash
# extract related position
caseVCF="/path/to/case/data.vcf.gz"
vcfForHail="/path/to/case/gnomADpc.subset.variants.vcf.gz"
# use hail_positions.GRCh38.chr.pos.tsv for GRCh38 v3
position="./example/hail_gnomAD/hail_positions.GRCh38.chr.pos.tsv"
bcftools view -R ${position} -Oz -o ${vcfForHail} ${caseVCF}

# then run gnomAD based prediction
loadingPath="/path/to/gnomad/loadings.ht"
rfModelPath="/path/to/gnomad/RF_fit.onnx"
vcfForHail="/path/to/case/gnomADpc.subset.variants.vcf.gz" 
build="GRCh38" # GRCh37 for v2 or GRCh38 for v3 and v4
outputFile=/path/to/output.gz # a gzipped file
threshold=0.75 # the threshold used to assign populations/ethnicities. 0.9 is used in gnomAD v2, 0.75 is used in gnomAD v3 and v4. If the probability is below the threhsold, other will be assigned. 
populationFile="casePopulation.txt"
python3 ./utilities/gnomADPCAndAncestry.py ${loadingPath} ${rfModelPath} ${vcfForHail} ${build} ${outputFile} ${threshold} ${populationFile}
```

#### Configuration to match the summary counts between cases and controls ####
There are two ways to specify the configuration of the analysis as follows.
##### Use the --ACANConfig and --caseGroup option.
The --ACANConfig option speficies a file which includes the following column headers: 
- controlAC: info field ID for the alternate allele count in controls
- controlAN: info field ID for the total allele count in controls
- controlAA: optional info field ID for the count of homozygous genotypes of the alternate alleles in controls
- caseGroup: the matched group ID in cases

The --caseGroup option specifies a file which stores the case group IDs for each sample. It contains at least two columns with a header. The first column is the sample ID and the second column is the group ID, e.g., the ethnicities. See ./example/1KG/stratified_config_gnomad.txt and ./example/1KG/ethnicity.txt for examples. This configuration requires full genotypes of the cases

##### Use the --ACANConfig option only.
The --ACANConfig specifies a file with the following column headers:
- controlAC: info field ID for the alternate allele count in controls 
- controlAN: info field ID for the total allele count in controls
- controlAA: optional info field ID for the count of homozygous genotypes of the alternate alleles in controls
- caseAC: info field ID for the alternate allele count in cases  
- caseAN: info field ID for the total allele count in cases
- caseAA: optional info field ID for the count of homozygous genotypes of the alternate alleles in cases

This configuration does not need the full genotypes of cases. It assumes only one line after the header in the configuration file for now.

#### Sex-stratified analysis for autosomal chromosomes
The idea is similar as population stratified analysis. For example, cases will be stratified not only by population, but also by sex. Then the stratified 
case group will be matched to the controlAC, controlAN, controlAA. See ./example/1KG/stratified_config_gnomadV3_nonneuro.sex.txt for an example. Note that stratifying by sex often reduce the number of controls to about a half for each group and may affect how small the AF threshold can be. 

#### Convert the vcf format to the GDS format ####
We convert the VCF files to the GDS format for easy extraction of information from R 
```bash
# case
vcfAnnotated=./example/1KG/samples.annotated.GT.vcf.gz
Rscript utilities/vcf2gds.R ${vcfAnnotated} ${vcfAnnotated}.gds 4
# control
gnomADAnnotated="./example/1KG/gnomAD.annotated.vcf.gz"
Rscript utilities/vcf2gds.R ${gnomADAnnotated} ${gnomADAnnotated}.gds 4
```

#### Defining variants of interest in the test ####
Variants of interest in the test can be defined based on annotations. There are three ways to specify the variants of interest. The first is the predefined variant group, such as annovar_pathogenic, annovar_LOF as listed in the following table.

| Predefined variant group | Filtering conditions |
| ------------------------ | -------------------- |
| annovar_pathogenic | (ExonicFunc.refGene %in% c("frameshift_deletion", "frameshift_insertion", "stopgain", "nonsynonymous_SNV")) |
| annovar_function | (ExonicFunc.refGene %in% c("frameshift_deletion", "frameshift_insertion", "stopgain", "nonsynonymous_SNV")) \| (Func.refGene == "splicing") |
| annovar_LOF | ExonicFunc.refGene %in% c("frameshift_deletion", "frameshift_insertion", "stopgain") \| (Func.refGene == "splicing") |
| annovar_missense | ExonicFunc.refGene == "nonsynonymous_SNV" |
| annovar_synonym | ExonicFunc.refGene == "synonymous_SNV" |
| annovar_splicing | Func.refGene == "splicing" |

For annovar_pathogenic, annovar_function, and annovar_missense, the option --minREVEL will be applied to missense variants to make sure the REVEL column is not missing and >= minREVEL.

The second way is to supply a text file to  the option --variantGroupCustom. The text file is a two column tab separated. The first column is the name of the variant group, the second column is the R logical expression which uses the annotation column names directly to define variants of interest. The name of the variant group is supplied to the option --variantGroup. The following is an example of defining two different variant groups based on ANNOVAR annotations:
```R
annovar_LOF (ExonicFunc.refGene %in% c("frameshift_deletion", "frameshift_insertion", "stopgain")) | (Func.refGene == "splicing") 
annovar_pathogenic_REVEL0.65  (ExonicFunc.refGene %in% c("frameshift_deletion", "frameshift_insertion", "stopgain")) | ( ExonicFunc.refGene == "nonsynonymous_SNV" & !is.na(REVEL) & REVEL >= 0.65 - 1e-6)
```

The third way is the most flexible way. It allows the user to define a custom variant group by supplying an R function file to the option --variantGroupCustom and the name of the custom function to the option variantGroup. The following is an example custom R function:
```R
customVariantSets = function(data) {
  # an example of a custom function to define variant sets as LOF, 
  # assuming annovar based annotation
  
  outIndex = (data[, "ExonicFunc.refGene"] %in% c("frameshift_deletion", 
                                                  "frameshift_insertion",
                                                  "stopgain")) | 
              data[, "Func.refGene"] == "splicing"
  
  return(outIndex)
}
```


#### Run against gnomAD ####
The function CoCoRV in the R package can be called in an R script. We also provide a simple command line wrapper (utilities/CoCoRV_wrapper.R). We have precomputed and stored the LD test result in a file with p < 0.1 (./example/1KG/all.p0.1.tsv.gz) for GRCh37. We assume the ethnicity has been assigned to each case sample and stored in a file (./example/1KG/ethnicity.txt) with the first column as the sample ID and the second column the ethnicity group. This ethnicity prediction can be done by combining the case data with the 1,000 Genomes data and extracting the top principle components, and then use a randome forest classifier to predict the ethnicity. Then different case ethnicity groups are matched to the gnomAD ethnicity specific summary counts, as specified in a configure file (./example/1KG/stratified_config_gnomad.txt). 
```bash
mkdir -p ./example/1KG/result

controlCount=./example/1KG/gnomAD.annotated.vcf.gz.gds
caseCount=./example/1KG/samples.annotated.GT.vcf.gz.gds
sampleList=./example/1KG/samples.txt
intersectBed=./example/1KG/intersect.coverage10x.bed.gz
variantExclude=./example/1KG/gnomAD.exclude.allow.segdup.lcr.v3.txt.gz
AFMax=1e-4
maxAFPopmax=1
variantMissing=0.1
ACANConfig=./example/1KG/stratified_config_gnomad.txt
ancestryFile=./example/1KG/ethnicity.txt
variantGroup="annovar_pathogenic" 
REVELThreshold=0.65
pLDControl=0.05
highLDVariantFile=./example/1KG/full_vs_gnomAD.p0.05.OR1.ignoreEthnicityInLD.rds
outputPrefix=./example/1KG/result/AFMax${AFMax}.${variantGroup}.${REVELThreshold}.excludeV3.LDv2

Rscript utilities/CoCoRV_wrapper.R \
  --sampleList ${sampleList} \
  --outputPrefix ${outputPrefix} \
  --AFMax ${AFMax} \
  --maxAFPopmax ${maxAFPopmax} \
  --bed ${intersectBed} \
  --variantMissing ${variantMissing} \
  --variantGroup ${variantGroup} \
  --removeStar \
  --ACANConfig ${ACANConfig} \
  --caseGroup ${ancestryFile} \
  --minREVEL ${REVELThreshold} \
  --variantExcludeFile ${variantExclude}  \
  --checkHighLDInControl \
  --pLDControl ${pLDControl} \
  --highLDVariantFile ${highLDVariantFile} \
  --fullCaseGenotype \
  ${controlCount} \
  ${caseCount}
```
Run the following to get the help information of the wrapper. 
```bash
Rscript utilities/CoCoRV_wrapper.R --help
```
Now the annotation data for cases and controls can be separated from the 
count data by using the option --caseAnnoGDSFile and --controlAnnoGDSFile for cases and controls, respectively. Loading all annotations can be slow, e.g., when using gnomAD V3 WGS summary counts, option --annotationUsed can be used to load just the annotations needed for the analysis, which can speed up the processing. 

#### QQ Plot and FDR for discrete counts ####
We provide a function qqplotHGAndFDR for estimating the inflation factor, generating the QQ plot based on sampled null P values, and calculate a resampling based FDR which is more powerful for discrete counts than the Benjamini-Hochberg method. We put a simple wrapper for the command line shell script usage for QQ plot and FDR calculation. For QQ plot only, 100 replications are often good; for either a box plot of lambdas under the null or resampling based FDR estimation, 1,000 replications are recommended.
```bash
outputFile=${outputPrefix}.association.tsv
# plot the dominant model with lambda estimation
Rscript utilities/QQPlotAndFDR.R ${outputFile} \
           ${outputFile}.dominant.nRep1000 --setID gene \
           --outColumns gene --n 1000 \
      --pattern "case.*Mutation.*_DOM$|control.*Mutation.*_DOM$" \
      --nullBoxplot --FDR
```
Run the following to get more information.
```bash
Rscript utilities/QQPlotAndFDR.R --help
```

#### Variant extraction for further manual check ####
If we find some genes interesting, it is better to further check whether the variants driving the association are high quality variants and there is no obvious confounding due to ancestries/ethnicities. The script utilities/postCheckCoCoRV.sh can help extract related QC and annotation information to help this check. 
```bash


# extract variants and sample IDs in cases of the top 10 genes
cocorvOut="./example/1KG/result/AFMax1e-4.annovar_pathogenic.0.65.excludeV3.LDv2.chr21.association.tsv"
variantInfoPrefix="./example/1KG/result/AFMax1e-4.annovar_pathogenic.0.65.excludeV3.LDv2.chr21"
casecontrol="case"
vcfAnnoPrefix="./example/1KG/chr"
vcfAnnoSuffix=".samples.annotated.vcf.gz"
vcfPrefix="./example/1KG/chr"
vcfSuffix=".samples.annotated.GT.vcf.gz"
outputFile="${cocorvOut}.${casecontrol}.variants.tsv"
fullGenotype=T
sampleList="./example/1KG/samples.txt"
build="GRCh37"
annotations="Gene.refGene,FILTER,Func.refGene,ExonicFunc.refGene,AAChange.refGene,REVEL"
topK=10
bash ./utilities/postCheckCoCoRV.sh ${cocorvOut} ${topK} ${variantInfoPrefix} \
  ${vcfAnnoPrefix} ${vcfAnnoSuffix} ${outputFile} ${casecontrol} ${fullGenotype} ${sampleList} ${build} ${annotations} ${vcfPrefix} ${vcfSuffix}

# extract variants in controls of the top 10 genes
cocorvOut="./example/1KG/result/AFMax1e-4.annovar_pathogenic.0.65.excludeV3.LDv2.chr21.association.tsv"
variantInfoPrefix="./example/1KG/result/AFMax1e-4.annovar_pathogenic.0.65.excludeV3.LDv2.chr21"
casecontrol="control"
vcfAnnoPrefix="./example/1KG/chr"
vcfAnnoSuffix=".gnomAD.annotated.reduced.vcf.gz"
vcfPrefix="./example/1KG/chr"
vcfSuffix=".gnomAD.annotated.reduced.vcf.gz"
outputFile="${cocorvOut}.${casecontrol}.variants.tsv"
fullGenotype=F
sampleList=NA
build="GRCh37"
annotations="Gene.refGene,FILTER,Func.refGene,ExonicFunc.refGene,AAChange.refGene,REVEL"
topK=10
bash ./utilities/postCheckCoCoRV.sh ${chrGeneList} ${topK} ${variantInfoPrefix} \
  ${vcfAnnoPrefix} ${vcfAnnoSuffix} ${outputFile} ${casecontrol} ${fullGenotype} ${sampleList} ${build} ${annotations} ${vcfPrefix} ${vcfSuffix}

```


### Contributors ###
* Wenan Chen
* Saima Sultana Tithi

### Contact ###

* Please contact Wenan Chen (chen.wenan@mayo.edu) for any questions

### License ###
MIT license
