# BamToBW_CoverageSummary
Converts bam file to bigwig file by first calculating per base depth of coverage using samtools depth and then storing the coverage information in a bigwig file format. Then calculates coverage summary statistics for a number of bigwig files and outputs a gzipped tab delimited file containing coverage statistics for each position.

# Installation
Requires python 3.7 or later and samtools 1.7 or later. Also requires python module pyBigWig installed in the system. It can be installed using the following command:
```bash
module load python/3.7.0
pip install pyBigWig
```

# Run bamToBW.sh script
The inputs of this script are:
1. bamFile : the full path of input bam file
2. outputPrefix : the output prefix with full path, the name of the output bigwig file will be ${outputPrefix}.bw
3. bedFile : the full path of the bed file which specifies the region to calculate coverage
4. samtools : the full path of samtools executable
5. referenceBuild : reference build, could be GRCh37 or GRCh38

The output of this script is a bigwig file with name ${outputPrefix}.bw

This script can be run using the following command:
```bash
./bamToBW.sh ${bamFile} ${outputPrefix} ${bedFile} ${samtools} ${referenceBuild}
```

# Run bwToCoverageSummary.py script:
The inputs of this script are:
1. bigwigFileList : the list of bigwig files for which we need to generate the coverage summary statistics. Each bigwig file name should have the full path.
2. bedFile : the full path of the bed file which specifies the region to calculate coverage
3. outputPrefix : the output prefix with full path

The output of this script will be a number of gzipped tab delimited files where each file will contain coverage summary statistics for all positions of one chromosome. For example, if the input bed file contains positions for chr1, chr2, and chr5, then there will be three output files with name ${outputPrefix}_chr1.tsv.gz, ${outputPrefix}_chr2.tsv.gz, and ${outputPrefix}_chr5.tsv.gz

This script can be run using the following command:
```bash
python bwToCoverageSummary.py -bw ${bigwigFileList} -bed ${bedFile} -out ${outputPrefix}
```

# Run scripts using sample inputs:
Some sample bam files and a sample bed file are given in the "sample-input" folder. Here, the input files are:
1. HG00096-subset.bam, HG00097-subset.bam, HG00099-subset.bam : Input bam files from where we want to calculate coverage summary
2. sample-bed.bed : Input bed file that specifies the region to calculate the covearge summary

A sample bash script named "sample-run-script.sh" is given which can be used to generate coverage summary for the given input files. This script will run bamToBW.sh script for the three input bam files and will generate three bigwig files with name HG00096.bw, HG00097.bw, and HG00099.bw and will save them in the "sample-output" folder. It will then run the bwToCoverageSummary.py script using the three bigwig files. As the sample input bed file contains poistions for chromosomes 1 and 2, it will generate two coverage summary files with name Coverage-summary_1.tsv.gz and Coverage-summary_2.tsv.gz and will save them in the "sample-output" folder.

