#!/bin/bash

set -eu -o pipefail

main() {
    # run bam to bigwig conversion script for three bam files given in sample-input folder
    # it will generate three bigwig files
    bamFile1="sample-input/HG00096-subset.bam"
    bamFile2="sample-input/HG00097-subset.bam"
    bamFile3="sample-input/HG00099-subset.bam"
    outputPrefix1="sample-output/HG00096"
    outputPrefix2="sample-output/HG00097"
    outputPrefix3="sample-output/HG00099"
    bedFile="sample-input/sample-bed.bed"
    samtools="utils/samtools"
    referenceBuild="GRCh37"

    bash bamToBW.sh ${bamFile1} ${outputPrefix1} ${bedFile} ${samtools} ${referenceBuild}
    bash bamToBW.sh ${bamFile2} ${outputPrefix2} ${bedFile} ${samtools} ${referenceBuild}
    bash bamToBW.sh ${bamFile3} ${outputPrefix3} ${bedFile} ${samtools} ${referenceBuild}

    ls sample-output/*.bw > sample-output/bwFiles.txt

    # run bwToCoverageSummary.py script which calculates coverage summary statistics for the three bigwig files
    bigwigFileList="sample-output/bwFiles.txt"
    outputPrefix="sample-output/Coverage-summary"

    python bwToCoverageSummary.py -bw ${bigwigFileList} -bed ${bedFile} -out ${outputPrefix}
}

main "$@"
